package com.famouscoader.apicallingmvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    private lateinit var productViewModel: ProductViewModel
    private lateinit var refreshNow: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        refreshNow = findViewById(R.id.refreshNow)
        productViewModel = ViewModelProvider(this)[ProductViewModel::class.java]

        // Observe the LiveData and update the UI
        productViewModel.productData.observe(this) { productData ->

            updateUI(productData)
        }
    }

    private fun updateUI(productData: ProductData?) {
        refreshNow.setOnClickListener {
            Toast.makeText(this, "" + productData?.brand_name, Toast.LENGTH_SHORT).show()

        }

    }

}